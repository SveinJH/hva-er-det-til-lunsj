import React from 'react';
import './App.scss';
import { DayList } from './components/DayList';

function App() {
  return (
    <div className="App">
      <DayList />
    </div>
  );
}

export default App;
