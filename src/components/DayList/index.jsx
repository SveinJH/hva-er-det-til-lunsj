import axios from 'axios';
import React, { Component } from 'react';
import { DayCard } from './partials/DayCard';
import {
  dinnerStrings,
  soupStrings,
  favourites,
} from '../../constants';
import './style.scss';

class DayList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      data: null,
    };

    this.checkType = (dish, keywords) => {
      for (let i = 0; i < keywords.length; i++) {
        if (dish.toLowerCase().startsWith(keywords[i].toLowerCase())) {
          return true;
        }
      }
      return false;
    };

    this.untangle = ds => {
      const dishes = [];
      for (let i = 0; i < ds.length; i++) {
        const d = ds[i];
        const food = d.replace(/^dagens/i, '')
          .replace(/:+/i, ' ')
          .replace(/^ +/, '');

        // Set to first word by default
        let type = food.replace(/ .*/, '');
        if (this.checkType(food, dinnerStrings)) {
          type = 'Varmrett';
        } else if (this.checkType(food, soupStrings)) {
          type = 'Suppe';
        }

        const dish = food.replace(/^.*? /, '').replace(/^ +/, '');

        if (dish) {
          const favourite = favourites.some(f => dish.toLowerCase().includes(f.toLowerCase()));

          dishes.push({ type, dish, favourite });
        }
      }
      return dishes;
    };
  }

  componentDidMount() {
    axios.get('https://i74qu6dp3m.execute-api.us-east-2.amazonaws.com/').then(
      result => {
        const data = result.data.days.map(d => ({
          day: d.day,
          dishes: this.untangle(d.dishes),
        }));

        this.setState({
          isLoaded: true,
          data,
        });
      },
      error => {
        this.setState({
          isLoaded: true,
          error,
        });
      },
    );
  }

  render() {
    const { isLoaded, data, error } = this.state;

    const dayData = data ? data.map(d => (
      <DayCard key={d.day} day={d.day} dishes={d.dishes} />
    )) : null;

    let display = isLoaded ? 'Noe gikk galt!' : 'Henter meny...';
    if (error) {
      display = error.message;
    } else if (dayData) {
      display = dayData;
    }

    return (
      <div className="day-list-component">
        {display}
      </div>
    );
  }
}

export { DayList };
