import React from 'react';
import PropTypes from 'prop-types';
import {
  dinnerStrings,
} from '../../../../constants';
import './style.scss';

const isToday = dayName => {
  // Create an array containing each day, starting with Sunday.
  const weekdays = [
    'Søndag', 'Mandag', 'Tirsdag', 'Onsdag', 'Torsdag', 'Fredag', 'Lørdag',
  ];
  // Use the getDay() method to get the day.
  const day = (new Date()).getDay();
  // Return the element that corresponds to that index.
  const currentDay = weekdays[day];

  return dayName.toLowerCase().includes(currentDay.toLowerCase());
};

const DayCard = ({ day, dishes }) => {
  dishes.sort((a, b) => a.type < b.type);

  let img = null;
  const mainDish = dishes.find(d => dinnerStrings.some(s => s === d.type.toLowerCase()));
  if (mainDish) {
    const url = 'https://images.hvaerdettillunsj.no/';
    const dishName = mainDish.dish.replace(/ /g, '_').replace(/[^a-zA-Z0-9æøåé_]/g, '');
    const src = `${url}${dishName}.png`;
    img = <img className="day-image" src={src} alt="Bilde av dagens hovedrett" />;
  }

  return (
    <div className={`day-card-wrapper${isToday(day) ? ' today' : ''}`}>
      {img}
      <div className="content-wrapper">
        <div className="day-card-header">
          {day}
        </div>
        <div className="day-card-body">
          <ul className="meal-list">
            {dishes.map(u => (
              <li className="meal-item-wrapper" key={u.type}>
                <div className="meal-header">{u.type}</div>
                <div className={`meal-body${u.favourite ? ' fav' : ''}`}>{u.dish}</div>
              </li>
            ))}
          </ul>
        </div>
      </div>
    </div>
  );
};

DayCard.defaultProps = {
  day: '',
  dishes: [],
};

DayCard.propTypes = {
  day: PropTypes.string,
  dishes: PropTypes.arrayOf(PropTypes.shape({
    type: PropTypes.string.isRequired,
    dish: PropTypes.string.isRequired,
    favourite: PropTypes.bool.isRequired,
  })),
};

export { DayCard };
