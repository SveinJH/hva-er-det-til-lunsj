export const dinnerStrings = [
  'varmrett',
  'hovedrett',
  'varmmat',
];

export const soupStrings = [
  'suppe',
];

export const favourites = [
  'pizza',
  'burger',
  'kjøttkaker',
];
